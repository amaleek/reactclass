import "./App.css";
import React, { useState } from "react";
import Todos from "./components/Todos";

function App() {
  // const state = {
  //   todos: [
  //     {
  //       id: 1,
  //       title: "First",
  //       completed: false,
  //     },
  //     {
  //       id: 2,
  //       title: "Second",
  //       completed: false,
  //     },
  //     {
  //       id: 3,
  //       title: "Third",
  //       completed: false,
  //     },
  //   ],
  // };
  const [state, setState] = useState([
    {
      id: 1,
      title: "First",
      completed: false,
    },
    {
      id: 2,
      title: "Second",
      completed: false,
    },
    {
      id: 3,
      title: "Third",
      completed: false,
    },
  ]);
  // Toggle completed
  const markComplete = (id) => {
    setState(
      state.map((todo) => {
        // console.log(todo.id);
        if (todo.id === id) {
          todo.completed = !todo.completed;
        }
        return todo;
      })
    );
  };
  const delTodo = (id) => {
    setState([...state.filter((todo) => todo.id !== id)]);
  };
  return (
    <>
      <div className="App">
        <Todos todos={state} markComplete={markComplete} delTodo={delTodo} />
      </div>
    </>
  );
}

export default App;
