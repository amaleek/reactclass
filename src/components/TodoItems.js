import React, { Component } from "react";
import PropTypes from "prop-types";
export class TodoItems extends Component {
  GetStyles = () => {
    return {
      textDecoration: this.props.pro.completed ? "line-through" : "none",
      padding: "10px",
      background: "#f4f4f4",
      borderBottom: "1px #ccc dotted",
    };
  };

  render() {
    const { id, title } = this.props.pro;
    return (
      <div style={this.GetStyles()}>
        <p>
          <input
            type="checkbox"
            onChange={this.props.markComplete.bind(this, id)}
          />{" "}
          {title}
          <button onClick={this.props.delTodo.bind(this,id)} style={styles.btnStyle}>x</button>
        </p>
      </div>
    );
  }
}
const styles ={
    btnStyle:{
        backgroundColor:"red",
        color:"white",
        border:"none",
        float:"right",
        padding:"5px 9px",
        borderRadius:"50%",
        cursor:"pointer"
    }
}
TodoItems.propTypes = {
  pro: PropTypes.object.isRequired,
};
