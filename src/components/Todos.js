import React, { Component } from "react";
import { TodoItems } from "./TodoItems";
import PropTypes from "prop-types";

export default class Todos extends Component {
  render() {
    // console.log(this.props.todos)
    return (
      <div>
        {React.Children.toArray(
          this.props.todos.map((todo) => (
            <>
              <TodoItems
                pro={todo}
                markComplete={this.props.markComplete}
                delTodo={this.props.delTodo}
              />
            </>
          ))
        )}
      </div>
    );
  }
}

Todos.propTypes={
  todos:PropTypes.array.isRequired
}
